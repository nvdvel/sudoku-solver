package nl.holdyfolds.spring.sudokusolver.solver;

import nl.holdyfolds.spring.sudokusolver.data.Cell;

import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Contains a subset hint. Can be used by the solver to generate moves to solve the sudoku.
 */
public class SubsetHint {
    /** The subset of values (single, pair, triple, quad). */
    private List<Integer> subset;

    /** The cells for which the subset occurs. */
    private List<Cell> cells;

    /** The cell group (column, row, square) in which the subset was found. Used to update other cells.*/
    private Set<Cell> cellGroup;

    /**
     * Creates and initialises subset hint.
     * @param subset subset of values.
     * @param cells cells that are part of the subset.
     * @param cellGroup group (column, row, square) that the subset was found in.
     */
    public SubsetHint(List<Integer> subset, List<Cell> cells, Set<Cell> cellGroup) {
        this.subset = subset;
        this.cells = cells;
        this.cellGroup = cellGroup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubsetHint that = (SubsetHint) o;
        return subset.equals(that.subset) && cells.equals(that.cells);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subset, cells);
    }

    public List<Integer> getSubset() {
        return subset;
    }

    public List<Cell> getCells() {
        return cells;
    }

    public Set<Cell> getCellGroup() {
        return cellGroup;
    }
}
