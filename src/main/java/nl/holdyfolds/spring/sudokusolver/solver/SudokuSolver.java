package nl.holdyfolds.spring.sudokusolver.solver;

import nl.holdyfolds.spring.sudokusolver.data.Cell;
import nl.holdyfolds.spring.sudokusolver.data.SquareSudoku;
import nl.holdyfolds.spring.sudokusolver.solver.move.Move;
import nl.holdyfolds.spring.sudokusolver.solver.move.MoveRemovePossibleValues;
import nl.holdyfolds.spring.sudokusolver.solver.move.MoveSetValue;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static nl.holdyfolds.spring.sudokusolver.solver.Utils.constructList;
import static nl.holdyfolds.spring.sudokusolver.solver.Utils.collectionMinus;

public class SudokuSolver {
    /** Groups cells into columns. */
    private List<Set<Cell>> columns = new ArrayList<>(SquareSudoku.SUDOKU_SIZE);

    /** Groups cells into rows. */
    private List<Set<Cell>> rows = new ArrayList<>(SquareSudoku.SUDOKU_SIZE);

    /** Groups cells into squares. */
    private List<Set<Cell>> squares = new ArrayList<>(SquareSudoku.SUDOKU_SIZE);

    private static final List<Integer> ALL_POSSIBLE_VALUES = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);

    /**
     * Creates and initializes the solver based on the provided sudoku.
     * Optionally also initializes the possible values of cells if necessary.
     * @param sudoku
     */
    public SudokuSolver(@NonNull SquareSudoku sudoku) {
        // Initialize list of columns, rows and squares with a new set of cells.
        for(int i=0; i < SquareSudoku.SUDOKU_SIZE; i++) {
            columns.add(new TreeSet<>());
            rows.add(new TreeSet<>());
            squares.add(new TreeSet<>());
        }

        // Distribute cell objects over columns, rows and squares. Cell objects are shared!
        sudoku.getCells().forEach(cell -> {
            columns.get(cell.getX()).add(cell);
            rows.get(cell.getY()).add(cell);
            squares.get(determineSquareIndex(cell.getX(), cell.getY())).add(cell);
        });

        // Re-initialise the possible values for each cell, if it's missing for a cell.
        if (sudoku.getCells().stream().anyMatch(cell -> cell.getPossibleValues() == null)) {
            sudoku.getCells().stream()
                    // Get all cells with a value.
                    .filter(cell -> cell.getValue() != null)
                    // Process the initial value in the metadata.
                    .forEach(cell -> processUpdate(cell, cell.getValue()));
        }
    }

    /**
     * Solve the sudoku iteratively.
     */
    public void solve() {
        List<Move> moveList = determineMoveList();
        while (moveList.size() > 0) {
            moveList.forEach(Move::performMove);
            moveList = determineMoveList();
        }
    }

    /**
     * Solve the sudoku, but only perform a maximum number of moves.
     * @param maxNrOfMoves maximum number of moves.
     */
    public void solveLimitedNumberOfMoves(int maxNrOfMoves) {
        List<Move> moveList = determineMoveList();
        while (moveList.size() > 0 && maxNrOfMoves > 0) {
            moveList.stream()
                    .limit(maxNrOfMoves)
                    .forEach(Move::performMove);

            maxNrOfMoves -= moveList.size();
            moveList = determineMoveList();
        }
    }

    /**
     * Generates a list of possible moves based on the current metadata.
     * @return list
     */
    private @NonNull List<Move> determineMoveList() {
        List<Move> moves = new ArrayList<>();

        // Determine naked subset hints
        Set<SubsetHint> nakedSubsetHints = columns.stream().flatMap(cellGroup -> determineNakedSubsetsForCellGroup(cellGroup).stream()).collect(Collectors.toSet());
        nakedSubsetHints.addAll(rows.stream().flatMap(cellGroup -> determineNakedSubsetsForCellGroup(cellGroup).stream()).collect(Collectors.toSet()));
        nakedSubsetHints.addAll(squares.stream().flatMap(cellGroup -> determineNakedSubsetsForCellGroup(cellGroup).stream()).collect(Collectors.toSet()));

        // Determine hidden subset hints
        Set<SubsetHint> hiddenSubsetHints = columns.stream().flatMap(cellGroup -> determineHiddenSubsetsForCellGroup(cellGroup).stream()).collect(Collectors.toSet());
        hiddenSubsetHints.addAll(rows.stream().flatMap(cellGroup -> determineHiddenSubsetsForCellGroup(cellGroup).stream()).collect(Collectors.toSet()));
        hiddenSubsetHints.addAll(squares.stream().flatMap(cellGroup -> determineHiddenSubsetsForCellGroup(cellGroup).stream()).collect(Collectors.toSet()));

        // For subsets of length 1, we can generate moves to set a value.
        Stream.concat(nakedSubsetHints.stream(), hiddenSubsetHints.stream())
                .filter(subsetHint -> subsetHint.getSubset().size() == 1)
                .forEach(subsetHint -> {
                    Cell cell = subsetHint.getCells().get(0);
                    Integer value = subsetHint.getSubset().get(0);
                    moves.add(new MoveSetValue(cell, value, () -> processUpdate(cell, value)));
                });

        // For naked subsets longer than 1, we can eliminate possible values from other cells (that are not part of the subset).
        nakedSubsetHints.stream()
                .filter(subsetHint -> subsetHint.getSubset().size() != 1)
                .forEach(subsetHint -> {
                    subsetHint.getCellGroup().stream()
                            // Look only at cells without a value.
                            .filter(cell -> cell.getValue() == null)
                            // Find other cells in cell group (that are not involved in the subset).
                            .filter(cell -> !subsetHint.getCells().contains(cell))
                            // Cells must contain at least one possible value from the subset (otherwise there's nothing to eliminate).
                            .filter(cell -> subsetHint.getSubset().stream().anyMatch(subsetValue -> cell.getPossibleValues().contains(subsetValue)))
                            // Create a move that eliminates possible values from the remaining cells.
                            .forEach(cell -> moves.add(new MoveRemovePossibleValues(cell, subsetHint.getSubset())));
                });

        // For hidden subsets longer than 1, we can eliminate possible values of the cells in the subset (making it a naked subset).
        hiddenSubsetHints.stream()
                .filter(subsetHint -> subsetHint.getSubset().size() != 1)
                .forEach(subsetHint -> subsetHint.getCells()
                        .forEach(subsetHintCell -> {
                            List<Integer> extraValues = collectionMinus(subsetHintCell.getPossibleValues(), subsetHint.getSubset(), Collectors.toList());
                            if (extraValues.size() > 0) {
                                moves.add(new MoveRemovePossibleValues(subsetHintCell, extraValues));
                            }
                        }));

        return moves;
    }

    /**
     * Determines a list of naked subset hints for a group of cells.
     * @param cellGroup groups of cells (row, columns, square).
     * @return
     */
    static @NonNull Set<SubsetHint> determineNakedSubsetsForCellGroup(@NonNull Set<Cell> cellGroup) {
        Set<SubsetHint> subsetHints = new HashSet<>();
        List<Integer> cellGroupValues = cellGroup.stream()
                .filter(cell -> cell.getValue() != null)
                .map(Cell::getValue).collect(Collectors.toList());

        // Get all values that are filled in. These are not possible anymore for other cells in the group.
        List<Integer> possibleValues = collectionMinus(ALL_POSSIBLE_VALUES, cellGroupValues, Collectors.toList());
        if (possibleValues.size() == 0) {
            // No values are possible anymore for cell group. The cell group is done.
            return subsetHints;
        }

        // Generate subsets of possible values.
        List<List<Integer>> subsets = generatePossibleValueSubsets(possibleValues);
        subsets.forEach(subset -> {
            // Get all cells that don't have a value.
            List<Cell> emptyCells = cellGroup.stream()
                    .filter(cell -> cell.getValue() == null)
                    .collect(Collectors.toList());

            // Get all cells whose possible values are a subset
            List<Cell> subsetCells = emptyCells.stream()
                    .filter(cell -> Utils.isSubListOf(cell.getPossibleValues(), subset))
                    .collect(Collectors.toList());

            // Number of cells should be equal to number of digits in subset (three for a triple, etc.)
            if (subsetCells.size() != subset.size()) {
                return;
            }

            // A naked subset is only useful if there are other empty cells.
            if (subsetCells.size() == emptyCells.size()) {
                return;
            }

            subsetHints.add(new SubsetHint(subset, subsetCells, cellGroup));
        });

        return subsetHints;
    }

    /**
     * Determines a list of hidden subset hints for a group of cells.
     * @param cellGroup groups of cells (row, columns, square).
     * @return
     */
    static @NonNull Set<SubsetHint> determineHiddenSubsetsForCellGroup(@NonNull Set<Cell> cellGroup) {
        Set<SubsetHint> subsetHints = new HashSet<>();
        List<Integer> cellGroupValues = cellGroup.stream()
                .filter(cell -> cell.getValue() != null)
                .map(Cell::getValue).collect(Collectors.toList());

        // Get all values that are filled in. These are not possible anymore for other cells in the group.
        List<Integer> possibleValues = collectionMinus(ALL_POSSIBLE_VALUES, cellGroupValues, Collectors.toList());
        if (possibleValues.size() == 0) {
            // No values are possible anymore for cell group. The cell group is done.
            return subsetHints;
        }

        // Generate subsets of possible values.
        List<List<Integer>> subsets = generatePossibleValueSubsets(possibleValues);
        subsets.forEach(subset -> {
            // Get all cells that don't have a value.
            List<Cell> emptyCells = cellGroup.stream()
                    .filter(cell -> cell.getValue() == null)
                    .collect(Collectors.toList());

            // Get cells for which any digit of the subset is possible.
            List<Cell> cells = emptyCells.stream()
                    .filter(cell -> cell.getValue() == null)
                    .filter(cell -> subset.stream().anyMatch(digit -> cell.getPossibleValues().contains(digit)))
                    .collect(Collectors.toList());

            // Number of cells should be equal to number of digits in subset.
            if (cells.size() != subset.size()) {
                return;
            }

            // For a hidden subset, one of the cells should contain a digit outside the subset (otherwise it's a naked subset).
            if (!cells.stream().anyMatch(cell -> cell.getPossibleValues().stream().anyMatch(possibleValue -> !subset.contains(possibleValue)))) {
                return;
            }

            subsetHints.add(new SubsetHint(subset, cells, cellGroup));
        });

        return subsetHints;
    }

    /**
     * Generate subsets of possible values that can be used to test for hidden/naked subsets.
     * @param possibleValues set of possible values that we want to generate subsets for.
     * @return list of subsets
     */
    static @NonNull List<List<Integer>> generatePossibleValueSubsets(@NonNull List<Integer> possibleValues) {
        List<List<Integer>> subsets = new ArrayList<>();
        if (possibleValues.size() == 1) {
            subsets.add(possibleValues);
            return subsets;
        }

        // Create a subset with just the first possible value.
        Integer value = possibleValues.get(0);
        subsets.add(Arrays.asList(value));

        // Create subsets for the rest of the possible values.
        List<List<Integer>> restSubsets = generatePossibleValueSubsets(possibleValues.subList(1, possibleValues.size()));
        subsets.addAll(restSubsets);

        // Concatenate the first possible value to the front of each rest subset.
        List<List<Integer>> newSubsets = restSubsets.stream()
                .map(subset -> constructList(value, subset))
                .collect(Collectors.toList());
        subsets.addAll(newSubsets);

        return subsets;
    }

    /**
     * Processes the update of a cell's value.
     * @param cell
     * @param newValue
     */
    public void processUpdate(@NonNull Cell cell, @Nullable Integer newValue) {
        // Determine possible values for cell.
        Set<Integer> possibleValueForCell;
        if (newValue == null) {
            // Clearing a value. Redetermine the possible values of the cell.
            possibleValueForCell = redeterminePossibleValuesForCell(cell);
        } else {
            // Setting a value. No possible values applicable anymore.
            possibleValueForCell = new TreeSet<>();
        }
        cell.setPossibleValues(new ArrayList<>(possibleValueForCell));

        // Redetermine possible values of sibling cells.
        Set<Cell> siblingCells = determineSiblingCells(cell);
        siblingCells.stream()
                // Get sibling cells without a value.
                .filter(siblingCell -> siblingCell.getValue() == null)
                // Redetermine the possible values.
                .forEach(siblingCell -> siblingCell.setPossibleValues(new ArrayList<>(redeterminePossibleValuesForCell(siblingCell))));
    }

    /**
     * Determines the set of values that are still possible for the cell.
     * @param cell
     * @return
     */
    private @NonNull Set<Integer> redeterminePossibleValuesForCell(@NonNull Cell cell) {
        Set<Integer> possibleValues = new TreeSet<>(ALL_POSSIBLE_VALUES);

        // Look at all the sibling cells of the provided cell.
        determineSiblingCells(cell).stream()
                // Get all sibling cells with a value.
                .filter(siblingCell -> siblingCell.getValue() != null)
                // Value of sibling cell is no longer a possible value for the provided cell.
                .forEach(siblingCell -> possibleValues.remove(siblingCell.getValue()));

        return possibleValues;
    }

    /**
     * Determine all cells that are in the same group (column/row/square) as the provided cell.
     * @param cell
     * @return set of sibling cells.
     */
    private @NonNull Set<Cell> determineSiblingCells(@NonNull Cell cell) {
        // Get all cells that are in the same group.
        Set<Cell> siblingCells = new TreeSet<>();
        siblingCells.addAll(columns.get(cell.getX()));
        siblingCells.addAll(rows.get(cell.getY()));
        siblingCells.addAll(squares.get(determineSquareIndex(cell.getX(), cell.getY())));

        // Don't return the cell itself.
        siblingCells.remove(cell);

        return siblingCells;
    }

    /**
     * Determines to which square a cell belongs, based on its coordinates.
     * The cells are laid out starting at the top-left,
     * going from left-to-right, top-to-bottom.
     * @param x
     * @param y
     * @return index that can be used in the list of squares.
     */
    private static int determineSquareIndex(int x, int y) {
        return (int) (Math.floor(x / 3) + (Math.floor(y / 3) * SquareSudoku.SQUARE_SIZE));
    }
}
