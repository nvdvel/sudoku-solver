package nl.holdyfolds.spring.sudokusolver.solver.move;

@FunctionalInterface
public interface Move {
    void performMove();
}
