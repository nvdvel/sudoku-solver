package nl.holdyfolds.spring.sudokusolver.solver;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collector;

/**
 * Contains some generic utility methods.
 */
public class Utils {
    /**
     * Constructs a new list by appending the head onto the tail.
     * @param head item that must be appended to tail.
     * @param tail tail of list.
     * @return list.
     * @param <E>
     */
    public static final @NonNull <E> List<E> constructList(@NonNull E head,@NonNull List<E> tail) {
        List<E> newList = new ArrayList<>(Arrays.asList(head));
        newList.addAll(tail);
        return newList;
    }

    /**
     * Tests if two lists of subsets are equal.
     * @param subsets1
     * @param subsets2
     * @return result
     * @param <E>
     */
    static <E> boolean equalsSubsets(@Nullable List<List<E>> subsets1, @Nullable List<List<E>> subsets2) {
        if (subsets1 == null || subsets2 == null || subsets1.size() != subsets2.size()) {
            return false;
        }

        return subsets1.stream().allMatch(subset -> subsets2.contains(subset));
    }

    /**
     * Tests if a list is a sublist of another list.
     * @param list1
     * @param list2
     * @return result
     * @param <E>
     */
    static <E> boolean isSubListOf(@NonNull List<E> list1, @NonNull List<E> list2) {
        if (list1.size() == 0) {
            return true;
        }
        return list1.stream().allMatch(list2::contains);
    }

    /**
     * Subtract the second collection from the first collection
     * and collect the result into a new collection.
     * @param collection1
     * @param collection2
     * @param collector
     * @return
     * @param <E>
     */
    static @NonNull <E, F> F collectionMinus(@NonNull Collection<E> collection1, @NonNull Collection<E> collection2, @NonNull Collector<E, ?, F> collector) {
        return collection1.stream()
                .filter(element -> !collection2.contains(element))
                .collect(collector);
    }
}
