package nl.holdyfolds.spring.sudokusolver.solver.move;

@FunctionalInterface
public interface Callback {
    void call();
}
