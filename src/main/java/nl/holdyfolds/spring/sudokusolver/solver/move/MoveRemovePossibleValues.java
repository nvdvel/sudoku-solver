package nl.holdyfolds.spring.sudokusolver.solver.move;

import nl.holdyfolds.spring.sudokusolver.data.Cell;

import java.util.Collection;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Move to remove possible values from a cell.
 */
public class MoveRemovePossibleValues implements Move {
    Logger logger = Logger.getLogger(MoveRemovePossibleValues.class.getName());

    /** Cell from which we want to remove possible values. */
    private Cell cell;

    /** Values that are not possible anymore for the cell. */
    private Collection<Integer> values;

    public MoveRemovePossibleValues(Cell cell, Collection<Integer> values) {
        this.cell = cell;
        this.values = values;
    }

    @Override
    public void performMove() {
        // If cell has gotten a value by now, don't perform any additional move.
        if (cell.getValue()!= null) {
            return;
        }

        logger.info(String.format("Remove possible values %s from cell %s (possible values %s)."
                , values.stream().map(value -> value.toString()).collect(Collectors.joining(","))
                , cell
                , cell.getPossibleValues().stream().map(value -> value.toString()).collect(Collectors.joining(","))));

        cell.getPossibleValues().removeAll(values);
    }
}
