package nl.holdyfolds.spring.sudokusolver.solver.move;

import nl.holdyfolds.spring.sudokusolver.data.Cell;

import java.util.logging.Logger;

/**
 * Move to set the value of a cell.
 */
public class MoveSetValue implements Move {
    Logger logger = Logger.getLogger(MoveSetValue.class.getName());

    /** Cell that we want to set the value for. */
    private Cell cell;

    /** New value for the cell. */
    private Integer value;

    /** Callback function to call after update (to recalculate the metadata after each move). */
    private Callback updateCallback;

    public MoveSetValue(Cell cell, Integer value, Callback updateCallback) {
        this.cell = cell;
        this.value = value;
        this.updateCallback = updateCallback;
    }

    @Override
    public void performMove() {
        // If cell has gotten a value by now, don't perform any additional move.
        if (cell.getValue()!= null) {
            return;
        }

        cell.setValue(value);
        logger.info("Set value " + cell);
        updateCallback.call();
    }
}
