package nl.holdyfolds.spring.sudokusolver.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.holdyfolds.spring.sudokusolver.data.Cell;
import nl.holdyfolds.spring.sudokusolver.data.SquareSudoku;
import nl.holdyfolds.spring.sudokusolver.solver.SudokuSolver;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/sudoku")
public class SudokuSolverController {
    /**
     * Gets an empty sudoku.
     * @return
     */
    @GetMapping("/empty")
    public Cell[] readEmptySudoku() {
        SquareSudoku sudoku = new SquareSudoku(Arrays.asList());
        SudokuSolver sudokuSolver = new SudokuSolver(sudoku);
        return sudoku.getCells().toArray(new Cell[0]);
    }

    /**
     * Gets an example of a sudoku.
     * @param difficulty
     * @return List of cells with an initial value. We don't return cells without a value.
     * @throws IOException
     */
    @GetMapping("/new")
    public Cell[] readExampleSudoku(@RequestParam(value = "difficulty", required = false) String difficulty) throws IOException {
        // Create sudoku object based on list of cells.
        List<Cell> cells = readExampleSudokuCells();
        SquareSudoku sudoku = new SquareSudoku(cells);

        // Initialize solver to calculate possible values for each sudoku cell.
        SudokuSolver sudokuSolver = new SudokuSolver(sudoku);
        return sudoku.getCells().toArray(new Cell[0]);
    }

    /**
     * Update possible values for sudoku, after a cell has been updated.
     * @param cells the cells of the sudoku (with out of date possible values).
     * @param x x coordinate of cell that was updated (allows for more efficient update).
     * @param y y coordinate of cell that was updated (allows for more efficient update).
     * @return Cells with the possible values updated.
     */
    @PostMapping("/cell/{x}/{y}/update")
    public Cell[] updatePossibleValues(@RequestBody List<Cell> cells, @PathVariable Integer x, @PathVariable Integer y) {
        // Create sudoku object based on list of cells.
        SquareSudoku sudoku = new SquareSudoku(cells);

        // Find the cell and update the value.
        Cell cell = sudoku.getCells().stream().filter(c -> c.getX() == x && c.getY() == y).findFirst().get();

        // Initialize solver to calculate possible values for each sudoku cell.
        SudokuSolver sudokuSolver = new SudokuSolver(sudoku);

        // Process the update to recalculate possible values for some cells.
        sudokuSolver.processUpdate(cell, cell.getValue());

        return sudoku.getCells().toArray(new Cell[0]);
    }

    /**
     * Solves the sudoku.
     * @param cells list of cells.
     * @return
     */
    @PostMapping("/solve/{maxNumberOfMoves}")
    public Cell[] solveSudoku(@RequestBody List<Cell> cells, @PathVariable Integer maxNumberOfMoves) {
        // Create sudoku object based on list of cells.
        SquareSudoku sudoku = new SquareSudoku(cells);

        // Initialize solver to calculate possible values for each sudoku cell.
        SudokuSolver sudokuSolver = new SudokuSolver(sudoku);

        // Solve sudoku.
        sudokuSolver.solveLimitedNumberOfMoves(maxNumberOfMoves);
        return sudoku.getCells().toArray(new Cell[0]);
    }

    /**
     * Solves the sudoku.
     * @param cells list of cells.
     * @return
     */
    @PostMapping("/solve")
    public Cell[] solveSudoku(@RequestBody List<Cell> cells) {
        // Create sudoku object based on list of cells.
        SquareSudoku sudoku = new SquareSudoku(cells);

        // Initialize solver to calculate possible values for each sudoku cell.
        SudokuSolver sudokuSolver = new SudokuSolver(sudoku);

        // Solve sudoku.
        sudokuSolver.solve();
        return sudoku.getCells().toArray(new Cell[0]);
    }

    /**
     * Reads sudoku cells for a working example sudoku.
     * @return list of cells with an initially filled value.
     * @throws IOException
     */
    private List<Cell> readExampleSudokuCells() throws IOException {
        try (InputStream is = new ClassPathResource("static/sudoku_geniaal_1.json").getInputStream()) {
            TypeReference<List<Cell>> cellListType = new TypeReference<List<Cell>>() {};
            ObjectMapper mapper = new ObjectMapper();
            List<Cell> cells = mapper.readValue(is, cellListType);
            return cells;
        }
    }
}
