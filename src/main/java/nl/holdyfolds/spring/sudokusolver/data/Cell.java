package nl.holdyfolds.spring.sudokusolver.data;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * Represents one cell as part of a sudoku puzzle.
 */
public class Cell implements Comparable<Cell>{
    private static final Comparator<Cell> comparator = Comparator.comparing(Cell::getY).thenComparing(Cell::getX);

    /** Coordinates of cell. */
    private int x,y;

    /** Value of cell ({@code null} if the cell is empty). */
    private Integer value;

    /** Possible values of cell. Can be {@code null} if possible values is unknown for cell. */
    private List<Integer> possibleValues;

    /**
     * Default constructor (for deserialization).
     */
    public Cell() {}

    /**
     * Create and initialise a cell.
     * @param x
     * @param y
     * @param value
     * @param possibleValues
     */
    public Cell(int x, int y, Integer value, List<Integer> possibleValues) {
        this.x = x;
        this.y = y;
        this.value = value;
        this.possibleValues = possibleValues;
    }

    /**
     * Creates a string representation of the cell including its coordinates and its value.
     * @return
     */
    public String toString() {
        return String.format("(%d,%d)=%d", x, y, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return x == cell.x && y == cell.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public int compareTo(Cell cell) {
        return comparator.compare(this, cell);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public List<Integer> getPossibleValues() {
        return possibleValues;
    }

    public void setPossibleValues(List<Integer> possibleValues) {
        this.possibleValues = possibleValues;
    }
}
