package nl.holdyfolds.spring.sudokusolver.data;

import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a 3x3 square sudoku puzzle.
 */
public class SquareSudoku {
    public static final int SQUARE_SIZE = 3;
    public static final int SUDOKU_SIZE = SQUARE_SIZE * SQUARE_SIZE;
    public static final int NR_OF_CELLS = SUDOKU_SIZE * SUDOKU_SIZE;

    /**
     * Cells of sudoku.
     * The order is from left-to-right, top-to-bottom (starting in upper left corner of puzzle).
     */
    private List<Cell> cells;

    /**
     * Creates and initializes sudoku based on a list of initially filled cells.
     * @param initialCells cells with an initial value. No initial values if {@code null}.
     */
    public SquareSudoku(@Nullable List<Cell> initialCells) {
        this.cells = new ArrayList<>(NR_OF_CELLS);

        // Initialize cells from top-to-bottom, left-to-right.
        for (int y = 0; y < SUDOKU_SIZE; y++) {
            for (int x = 0; x < SUDOKU_SIZE; x++) {
                this.cells.add(determineCellIndex(x, y), new Cell(x, y, null, null));
            }
        }

        // Set initial values of cells.
        if (initialCells != null) {
            initialCells.forEach(initialCell -> {
                Cell cell = this.cells.get(determineCellIndex(initialCell.getX(), initialCell.getY()));
                cell.setValue(initialCell.getValue());
                cell.setPossibleValues(initialCell.getPossibleValues());
            });
        }
    }

    /**
     * Calculates the list-index for a cell based on its coordinates.
     * @param x
     * @param y
     * @return index
     */
    private int determineCellIndex(int x, int y) {
        return x + y * SUDOKU_SIZE;
    }

    public List<Cell> getCells() {
        return cells;
    }
}
