# NgSudokuSolver

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.1.2.

Generate new project in current directory:
> ng new ng-sudoku-solver --prefix ngs --style scss --directory .

Angular Material UI components (https://material.angular.io) with a custom theme (https://material.angular.io/guide/theming#defining-a-theme):
> ng add @angular/material --skip-confirmation

Angular Flex-Layout (https://github.com/angular/flex-layout):
> npm i @angular/flex-layout @angular/cdk

## Project composition

* Angular Material UI components (https://material.angular.io).

* Custom theming of components: 
  * https://material.angular.io/guide/theming, and 
  * https://material.io/design/material-theming/implementing-your-theme.html.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
