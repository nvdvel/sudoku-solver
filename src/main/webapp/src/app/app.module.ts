import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";
import {RouterModule} from "@angular/router";
import {routes} from "./app.routings";
import {MatIconModule} from "@angular/material/icon";
import {MatMenuModule} from "@angular/material/menu";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatGridListModule} from "@angular/material/grid-list";
import {ValueSelectorComponent} from './value-selector/value-selector.component';
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatDialogModule} from "@angular/material/dialog";
import {HttpClientModule} from "@angular/common/http";
import {SudokuComponent} from './sudoku/sudoku.component';
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatListModule} from "@angular/material/list";

@NgModule({
  declarations: [
    AppComponent,
    ValueSelectorComponent,
    SudokuComponent
  ],
    imports: [
        BrowserModule,

        HttpClientModule,

        // Import Angular Flex Layout.
        FlexLayoutModule,

        // Import Angular Material UI components (and browser animations for Angular Material).
        MatToolbarModule,
        MatButtonModule,
        MatIconModule,
        MatMenuModule,
        MatGridListModule,
        MatButtonToggleModule,
        MatDialogModule,
        MatCheckboxModule,
        MatExpansionModule,
        BrowserAnimationsModule,

        RouterModule.forRoot(routes),
        MatListModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
