import {Component, Input, ViewEncapsulation} from '@angular/core';
import {ValueSelectorComponent} from "../value-selector/value-selector.component";
import {MatDialog} from "@angular/material/dialog";
import {SudokuService} from "../shared/service/sudoku.service";
import {Cell} from "../shared/service/data/Cell";

@Component({
  selector: 'ngs-sudoku',
  templateUrl: './sudoku.component.html',
  styleUrls: ['./sudoku.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SudokuComponent {
  @Input() cells!: Cell[];
  @Input() showCandidates: boolean = false;
  @Input() printMode: boolean = false;

  constructor(public dialog: MatDialog, public sudokuService: SudokuService) {}

  /**
   * Show modal dialog to select or change the value for a sudoku cell.
   * @param cell
   */
  chooseValue(cell: Cell): void {
    // Open value selector component in the dialog.
    let dialogRef = this.dialog.open(ValueSelectorComponent, {
      // Provide the value of the cell as data to the component.
      data: cell.value,

      // Don't restore focus to button that opened the modal (prevents residual ripple effect after closing modal).
      restoreFocus: false,

      // Custom CSS class for overlay pane.
      panelClass: "value-chooser-panel"
    });

    // Update the value of the cell.
    dialogRef.afterClosed().subscribe(result => this.sudokuService.update(cell, result));
  }

  /**
   * Create CSS for cell that determines the borders
   * @param cell
   */
  createCellCSS(cell: Cell): Record<string, boolean> {
    return {
      borderRight: cell.x === 2 || cell.x === 5,
      borderBottom: cell.y === 2 || cell.y === 5,
      borderLeft: cell.x % 3 != 0 && cell.x != 0,
      borderTop: cell.y % 3 != 0 && cell.y != 0
    }
  }
}
