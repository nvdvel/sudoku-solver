import { Route } from '@angular/router';
import {AppComponent} from "./app.component";

export const routes: Route[] = [
  {
    path: '',
    // loadChildren: () => import('./app.module').then(m => m.AppModule)
    component: AppComponent
  }
];
