import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'ngs-value-selector',
  templateUrl: './value-selector.component.html',
  styleUrls: ['./value-selector.component.scss']
})
export class ValueSelectorComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ValueSelectorComponent>,
              @Inject(MAT_DIALOG_DATA) public data: number) { }

  ngOnInit(): void {
    // Escape event should cancel the value selection.
    this.dialogRef.keydownEvents().subscribe(event => {
      if (event.key === "Escape") {
        this.onCancel();
      }
    });

    // Clicking on the backdrop also cancels the value selection.
    this.dialogRef.backdropClick().subscribe(event => this.onCancel());
  }

  /**
   * Cancels the selection of a value.
   */
  onCancel(): void {
    // Pass the original data back, because we are cancelling the selection.
    this.dialogRef.close(this.data);
  }
}
