import {Component} from '@angular/core';
import {SudokuService} from "./shared/service/sudoku.service";

@Component({
  selector: 'ngs-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ng-sudoku-solver';
  showHints: boolean = false;
  printMode: boolean = false;
  showJson: boolean = false;

  constructor(public sudokuService: SudokuService) {}
}
