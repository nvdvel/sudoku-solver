import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map, Observable, of} from "rxjs";
import {Cell} from "./data/Cell";

@Injectable({
  providedIn: 'root'
})
export class SudokuService {
  private cells?: Cell[];

  constructor(private httpClient: HttpClient) {}

  /**
   * Requests a sudoku puzzle.
   * @return {Observable<Cell[]>} We return all cells (with and without value).
   */
  getSudoku(): Observable<Cell[]> {
    // Return cached sudoku object.
    if (typeof this.cells !== 'undefined') {
      return of(this.cells);
    }

    // Retrieve new sudoku.
    return this.httpClient.get<Cell[]>("http://localhost:8080/api/sudoku/new").pipe(
      // Also cache the cells in the service.
      map(cells => this.cells = cells));
  }

  /**
   * Updates the value of the cell.
   * @param cell
   * @param value if the value is undefined, we clear the value of the cell.
   */
  update(cell: Cell, value?: number): void {
    if (typeof this.cells === 'undefined') {
      return;
    }

    // Set the cell value.
    cell.value = value;

    // Let the solver recalculate the possible values for some cells.
    this.httpClient
      .post<Cell[]>(`http://localhost:8080/api/sudoku/cell/${cell.x}/${cell.y}/update`, this.cells)
      .subscribe((cells: Cell[]) => this.cells = cells);
  }

  /**
   * Deletes the current sudoku.
   */
  delete(): void {
    this.httpClient.get<Cell[]>("http://localhost:8080/api/sudoku/empty")
      .subscribe(cells => this.cells = cells);
  }

  /**
   * Resets the sudoku to the initial state.
   */
  reset(): void {
    this.httpClient.get<Cell[]>("http://localhost:8080/api/sudoku/new")
      .subscribe(cells => this.cells = cells);
  }

  /**
   * Performs exactly one move.
   */
  solveOneMove(): void {
    if (typeof this.cells === 'undefined') {
      return;
    }

    this.httpClient
      .post<Cell[]>("http://localhost:8080/api/sudoku/solve/1", this.cells)
      .subscribe((cells: Cell[]) => this.cells = cells);
  }

  /**
   * Solve as much of the sudoku as possible.
   */
  solve(): void {
    if (typeof this.cells === 'undefined') {
      return;
    }

    this.httpClient
      .post<Cell[]>("http://localhost:8080/api/sudoku/solve", this.cells)
      .subscribe((cells: Cell[]) => this.cells = cells);
  }

  /**
   * Requests a JSON string of the current Sudoku, based on a simplified representation of a cell.
   * Each returned cell, will only have its coordinates (x, y) and value filled.
   * @return {string} JSON string with the list of all cells that have a value.
   */
  getSimpleJSON(): string {
    if (typeof this.cells === 'undefined') {
      return "";
    }

    // Create JSON string.
    return JSON.stringify(this.cells
      // Get cells with a value.
      .filter(cell => typeof cell.value !== 'undefined')
      // Create simplified cell representation.
      .map(cell => ({x:cell.x, y:cell.y, value:cell.value})));
  }
}
