export class Cell {
    readonly x: number;
    readonly y: number;
    value?: number;
    possibleValues?: number[];
    print: boolean = false;

    constructor(x: number, y: number, value?: number, possibleValues?: number[]) {
        this.x = x;
        this.y = y;
        this.value = value;
        this.possibleValues = possibleValues;
    }

    toString(): string {
      return `(${this.x}, ${this.y})=${this.value ?? ""}`;
    }
}
