package nl.holdyfolds.spring.sudokusolver.controller;

import nl.holdyfolds.spring.sudokusolver.data.Cell;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import java.io.IOException;

public class SudokuSolverControllerTest {
    @Test
    public void testGetSudoku() throws IOException {
        SudokuSolverController controller = new SudokuSolverController();
        Cell[] cells = controller.readExampleSudoku(null);
        Assertions.assertAll(() -> Assertions.assertTrue(cells.length != 0));
    }
}
