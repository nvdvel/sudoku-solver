package nl.holdyfolds.spring.sudokusolver.solver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.holdyfolds.spring.sudokusolver.data.Cell;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Tag("fast")
public class SudokuSolverTest {
    @Test
    @DisplayName("Test generating subsets of possible values.")
    public void testGeneratePossibleValueSubsets() {
        List<List<Integer>> subsets1 = SudokuSolver.generatePossibleValueSubsets(Arrays.asList(1));
        List<List<Integer>> result1 = Arrays.asList(Arrays.asList(1));
        assertTrue(Utils.equalsSubsets(subsets1, result1));

        List<List<Integer>> subsets2 = SudokuSolver.generatePossibleValueSubsets(Arrays.asList(1, 2));
        List<List<Integer>> result2 = Arrays.asList(Arrays.asList(1), Arrays.asList(2), Arrays.asList(1, 2));
        assertTrue(Utils.equalsSubsets(subsets2, result2));

        List<List<Integer>> subsets3 = SudokuSolver.generatePossibleValueSubsets(Arrays.asList(1, 2, 3));
        List<List<Integer>> result3 = Arrays.asList(
                Arrays.asList(1), Arrays.asList(2), Arrays.asList(3),
                Arrays.asList(2, 3), Arrays.asList(1, 2), Arrays.asList(1, 3),
                Arrays.asList(1, 2, 3));
        assertTrue(Utils.equalsSubsets(subsets3, result3));
    }

    @Test
    public void testDetermineSubsetHintsForGroup() throws JsonProcessingException {
        String cellGroup1Str = "[{\"x\":3, \"y\":3, \"possibleValues\":[2,4,9]}, {\"x\":4, \"y\":3, \"value\":1}             , {\"x\":5, \"y\":3, \"value\":6},"
                             + " {\"x\":3, \"y\":4, \"possibleValues\":[2,4,9]}, {\"x\":4, \"y\":4, \"possibleValues\":[2,9]}, {\"x\":5, \"y\":4, \"possibleValues\":[3,5,9]},"
                             + " {\"x\":3, \"y\":5, \"value\":7}               , {\"x\":4,\"y\":5,\"value\":8}               , {\"x\":5, \"y\":5, \"possibleValues\":[3,5,9]}]";
        Set<Cell> cellGroup1 = parseCellGroupJSON(cellGroup1Str);

        List<SubsetHint> hints = new ArrayList<>(SudokuSolver.determineNakedSubsetsForCellGroup(cellGroup1));
        assertTrue(hints.size() == 1);
        assertTrue(hints.get(0).getSubset().equals(Arrays.asList(2, 4, 9)));
        assertTrue(hints.get(0).getCells().equals(
                Arrays.asList(new Cell(3, 3, null, null),
                        new Cell(4, 4, null, null),
                        new Cell(3, 4, null, null))));
    }

    private static Set<Cell> parseCellGroupJSON(String cellGroupJSON) throws JsonProcessingException {
        TypeReference<List<Cell>> cellListType = new TypeReference<List<Cell>>() {};
        ObjectMapper mapper = new ObjectMapper();
        List<Cell> cells = mapper.readValue(cellGroupJSON, cellListType);
        return new HashSet<>(cells);
    }
}
