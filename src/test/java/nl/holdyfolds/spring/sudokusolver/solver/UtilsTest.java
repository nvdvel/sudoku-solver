package nl.holdyfolds.spring.sudokusolver.solver;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Tag("fast")
public class UtilsTest {
    @Test
    @DisplayName("Test the internal equalsSubset method.")
    public void testEqualsSubset() {
        assertTrue(Utils.equalsSubsets(
                Arrays.asList(Arrays.asList(1)),
                Arrays.asList(Arrays.asList(1))
        ));

        assertTrue(Utils.equalsSubsets(
                Arrays.asList(Arrays.asList(1), Arrays.asList(2)),
                Arrays.asList(Arrays.asList(1), Arrays.asList(2))
        ));

        assertTrue(Utils.equalsSubsets(
                Arrays.asList(Arrays.asList(1), Arrays.asList(2, 3)),
                Arrays.asList(Arrays.asList(1), Arrays.asList(2, 3))
        ));

        assertTrue(Utils.equalsSubsets(
                Arrays.asList(Arrays.asList(1, 2), Arrays.asList(2, 3)),
                Arrays.asList(Arrays.asList(1, 2), Arrays.asList(2, 3))
        ));

        assertTrue(Utils.equalsSubsets(
                Arrays.asList(Arrays.asList(1, 2, 3), Arrays.asList(2, 3)),
                Arrays.asList(Arrays.asList(1, 2, 3), Arrays.asList(2, 3))
        ));
    }

    @Test
    @DisplayName("Test the internal testSubListOf method.")
    public void testSubListOf() {
        assertTrue(Utils.isSubListOf(Arrays.asList(), Arrays.asList()));
        assertTrue(Utils.isSubListOf(Arrays.asList(), Arrays.asList(1)));
        assertTrue(Utils.isSubListOf(Arrays.asList(1), Arrays.asList(1)));
        assertTrue(Utils.isSubListOf(Arrays.asList(1), Arrays.asList(1,2)));
        assertTrue(Utils.isSubListOf(Arrays.asList(1,2), Arrays.asList(1,2)));
        assertFalse(Utils.isSubListOf(Arrays.asList(1,2,3), Arrays.asList(1,2)));
    }
}
