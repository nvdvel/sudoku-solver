Requirements to build project:

* JDK11
* Gradle
* Node.js
* NPM
* Angular CLI (npm install -g @angular/cli)

To build/deploy the project:

1. Clone repository.
2. Execute `npm install` in the location of the Angular frontend (src/main/webapp).
3. Build and serve Spring-Boot application with `./gradlew bootRun`.
4. Build and serve Angular frontend with `ng serve`.
